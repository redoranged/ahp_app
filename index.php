<?php
require_once 'init.php';
require_once 'controller/auth.php';

// Controller Setup
if(isset($_POST['model']))
{
    require_once 'controller/'.$_POST['model'].'.php';
}

$page = 'home';
if(isset($_GET['page']))
{
    $page = $_GET['page'];
}
$subPage = 'home';
if(isset($_GET['subPage']))
{
    $subPage = $_GET['subPage'];
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>AHP Application</title>

    <!-- Favicon -->
    <link rel="icon" type="image/png" href="<?=url('/favicon.png')?>"/>
    <link rel="icon" type="image/png" href="<?=url('/favicon.png')?>"/>

    <!-- Bootstrap -->
    <link rel="stylesheet" href="<?=url('/dist/bootstrap/css/bootstrap.min.css')?>">
    
    <!-- Datatable -->
    <link rel="stylesheet" href="<?=url('/dist/datatable/datatables.min.css')?>">

    <!-- MyStyle -->
    <link rel="stylesheet" href="<?=url('/dist/style.css')?>">

    <!-- JQuery -->
    <script src="<?=url('/dist/jquery-3.5.1.js')?>"></script>

    <!-- Popper -->
    <script src="<?=url('/dist/popper/popper.min.js')?>"></script>

    <!-- Bootstrap -->
    <script src="<?=url('/dist/bootstrap/js/bootstrap.min.js')?>"></script>

    <!-- Datatable -->
    <script src="<?=url('/dist/datatable/datatables.min.js')?>"></script>

    <!-- MyScript -->
    <script src="<?=url('/dist/script.js')?>"></script>
</head>
<body>
    <nav class="navbar navbar-expand-lg fixed-top navbar-dark bg-dark">
        <a class="navbar-brand mr-auto mr-lg-0" href="<?=url('/')?>">Konveksi Brother Company</a>
        <button class="navbar-toggler p-0 border-0" type="button" data-toggle="offcanvas">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="navbar-collapse offcanvas-collapse" id="navbarsExampleDefault">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item <?= ($page=='home') ? 'active' : '' ?>">
                    <a class="nav-link" href="<?=url('/')?>">Home <?= ($page=='home') ? '<span class="sr-only">(current)</span>' : '' ?></a>
                </li>
                <li class="nav-item <?= ($page=='spk' || $page=='spk.form' || $page=='spk.rekomendasi') ? 'active' : '' ?>">
                    <a class="nav-link" href="<?=url('?page=spk')?>">SPK <?= ($page=='spk') ? '<span class="sr-only">(current)</span>' : '' ?></a>
                </li>
                <li class="nav-item <?= ($page=='contact') ? 'active' : '' ?>">
                    <a class="nav-link" href="<?=url('?page=contact')?>">Contact <?= ($page=='contact') ? '<span class="sr-only">(current)</span>' : '' ?></a>
                </li>
                <?php
                    if(!$session->isLogedIn())
                    {
                ?>
                <li class="nav-item <?= ($page=='login') ? 'active' : '' ?>">
                    <a class="nav-link" href="<?=url('?page=login')?>">Login <?= ($page=='login') ? '<span class="sr-only">(current)</span>' : '' ?></a>
                </li>
                <?php
                    }else{
                ?>
                <li class="nav-item <?= ($page=='spk.setting' || $page=='kriteria.form' || $page=='kriteria.perbandingan' || contains('sub.kriteria', $page) || contains('jenis_kain', $page)) ? 'active' : '' ?>">
                    <a class="nav-link" href="<?=url('?page=spk.setting')?>">SPK Setting <?= ($page=='spk.setting' || $page=='kriteria.form') ? '<span class="sr-only">(current)</span>' : '' ?></a>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <?=$session->name?>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item" href="<?=url('?page=user')?>">Admin Setting</a>
                        <div class="dropdown-divider"></div>
                        <form action="<?=url('/?logout=true')?>" method="POST">
                            <input type="hidden" name="user_id" value="<?=$session->user_id?>">
                            <button class="dropdown-item" type="submit">Logout</button>
                        </form>
                    </div>
                </li>
                <?php
                    }
                ?>
            </ul>
        </div>
    </nav>
    
    <?php 
    if($page=='spk.setting' || $page=='kriteria.form' || $page=='kriteria.perbandingan' || contains('sub.kriteria', $page) || contains('jenis_kain', $page) || contains('pertanyaan', $page))
    {
    ?>
    <div class="nav-scroller bg-white shadow-sm">
        <nav class="nav nav-underline">
            <a class="nav-link <?= ($subPage=='home') ? 'active' : '' ?>" href="<?=url('?page=spk.setting')?>">Home</a>
            <a class="nav-link <?= ($subPage=='kriteria') ? 'active' : '' ?>" href="<?=url('?page=spk.setting&subPage=kriteria')?>">Kriteria</a>
            <a class="nav-link <?= ($subPage=='jenis_kain') ? 'active' : '' ?>" href="<?=url('?page=spk.setting&subPage=jenis_kain')?>">Jenis Kain</a>
        </nav>
    </div>
    <?php
    }
    ?>

    <main role="main" class="container">
        <div class="p-3">
            <!-- Alert -->
                <!-- Error -->
                <?php
                    if($session->issetSession('error'))
                    {
                ?>
                    <div class="alert alert-danger alert-dismissible fade show" role="alert">
                        <strong>Error!</strong> <?=$session->getSession('error')?>
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                <?php
                        $session->unsetSession('error');
                    }
                ?>
                <!-- Warning -->
                <?php
                    if($session->issetSession('warning'))
                    {
                ?>
                    <div class="alert alert-warning alert-dismissible fade show" role="alert">
                        <strong>Warning!</strong> <?=$session->getSession('warning')?>
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                <?php
                        $session->unsetSession('warning');
                    }
                ?>
                <!-- Success -->
                <?php
                    if($session->issetSession('success'))
                    {
                ?>
                    <div class="alert alert-success alert-dismissible fade show" role="alert">
                        <strong>Success!</strong> <?=$session->getSession('success')?>
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                <?php
                        $session->unsetSession('success');
                    }
                ?>

            <!-- Page Setup -->
            <?php
                if($session->isLogedIn())
                {
                    if($page == 'login')
                    {
                        header('location:'.url('/'));
                    }
                    if($page == 'spk.setting')
                    {
                        $page = $page.'.'.$subPage;
                    }
                }else{
                    if($page == 'spk.setting' || $page == 'kriteria.form' || $page=='kriteria.perbandingan' || contains('sub.kriteria', $page))
                    {
                        header('location:'.url('/'));
                    }
                }
                include autoload($page);
            ?>
        </div>
    </main>
</body>
</html>
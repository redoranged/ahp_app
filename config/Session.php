<?php
class Session
{
    public $user_id;
    public $name;
    public $username;

    function __construct()
    {
        session_start();

        $this->checkUser();
    }

    public function isLogedIn()
    {
        if(isset($this->user_id)
            && isset($this->name)
            && isset($this->username))
        {
            return true;
        }else{
            return false;
        }
    }

    function checkUser()
    {
        if($this->issetSession('user_id')
            && $this->issetSession('name')
            && $this->issetSession('username'))
        {
            $this->user_id = $this->getSession('user_id');
            $this->name = $this->getSession('name');
            $this->username = $this->getSession('username');
        }
    }

    public function destroyUser($user_id)
    {
        if($user_id != $this->user_id)
        {
            $this->setSession('error', 'Cannot Logout with diferent User!');
            return false;
        }
        $this->unsetSession('user_id');
        $this->unsetSession('name');
        $this->unsetSession('username');

        $this->user_id = null;
        $this->name = null;
        $this->username = null;
    }

    public function setUser($user_id, $name, $username)
    {
        $this->user_id = $user_id;
        $this->name = $name;
        $this->username = $username;

        $this->setSession('user_id', $this->user_id);
        $this->setSession('name', $this->name);
        $this->setSession('username', $this->username);
    }

    public function setSession($key, $value)
    {
        $_SESSION[$key] = $value;
    }

    public function getSession($key)
    {
        return $_SESSION[$key];
    }

    public function unsetSession($key)
    {
        unset($_SESSION[$key]);
    }

    public function issetSession($key)
    {
        if(isset($_SESSION[$key]))
        {
            return true;
        }else{
            return false;
        }
    }
}
?>
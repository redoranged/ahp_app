<?php
    /**
     * Functions
     */

    function url($path = '/')
    {
        return URL.$path;
    }

    function autoload($page)
    {
        return ROOT.'/page//'.$page.'.php';
    }

    function getRequest()
    {
        $request = [];
        foreach ($_POST as $key => $value) {
            if(is_array($value)){
                foreach ($value as $key_child => $val) {
                    $request[htmlspecialchars($key)][$key_child] = htmlspecialchars($val);
                }
            }else{
                $request[htmlspecialchars($key)] = htmlspecialchars($value);
            }
        }
        return $request;
    }

    function contains($needle, $haystack)
    {
        return strpos($haystack, $needle) !== false;
    }
?>
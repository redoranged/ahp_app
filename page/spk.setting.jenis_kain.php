<?php
    $jenis_kain = new JenisKain();

    $jenis_kains = $jenis_kain->select();
?>
<!-- Breadcrumb -->
<ol class="breadcrumb bg-white">
    <li class="breadcrumb-item"><a href="<?=url('?page=spk.setting')?>">SPK Setting</a></li>
    <li class="breadcrumb-item active">Jenis Kain</li>
</ol>
<!-- Content -->
<div class="card">
    <div class="card-body">
        <div class="row">
            <div class="col-lg-6 col-sm-12">
                <div class="btn-group" role="group">
                    <a href="<?=url('?page=jenis_kain.form&subPage=jenis_kain')?>" class="btn btn-sm btn-outline-success">Tambah Jenis Kain</a>
                </div>
            </div>
        </div>
        <div class="row mt-3">
            <div class="col-12">
                <table class="table table-bordered table-hover" id="tableJenisKain">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Jenis Kain</th>
                            <th>Keterangan</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        if(count($jenis_kains) > 0)
                        {
                            foreach($jenis_kains as $key => $jenis_kain)
                            {
                        ?>
                        <tr>
                            <td><?=$key+1?></td>
                            <td><?=$jenis_kain['jenis_kain']?></td>
                            <td><?=$jenis_kain['keterangan']?></td>
                            <td>
                                <form action="?page=spk.setting&subPage=jenis_kain" method="POST">
                                    <input type="hidden" name="model" value="jenisKain"/>
                                    <input type="hidden" name="jenis_kain_id" value="<?=$jenis_kain['jenis_kain_id']?>"/>
                                    <div class="btn-group" role="group">
                                        <a href="<?=url('?page=jenis_kain.form&subPage=jenis_kain&id='.$jenis_kain['jenis_kain_id'])?>" class="btn btn-sm btn-info">Edit</a>
                                        <button type="submit" name="delete" class="btn btn-sm btn-danger" onclick="return confirm('Hapus JenisKain?')">Hapus</button>
                                        <a href="<?=url('?page=jenis_kain.nilai_kriteria&subPage=jenis_kain&id='.$jenis_kain['jenis_kain_id'])?>" class="btn btn-sm btn-warning">Nilai Kain</a>
                                    </div>
                                </form>
                            </td>
                        </tr>
                        <?php
                            }
                        }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function(){
        $('#tableJenisKain').DataTable();
    })
</script>
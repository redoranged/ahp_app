<?php
    // Form Setup
    $kriteria_id = '';
    $kriteria = new Kriteria();
    $subKriteria = new SubKriteria();
    $nilaiBobotSubKriteria = new NilaiBobotSubKriteria();
    if(isset($_GET['parent_id']))
    {
        $kriteria_id = $_GET['parent_id'];
        $kriteria = $kriteria->find($kriteria_id);
        if(!$kriteria)
        {
            $session->setSession('warning', 'Kriteria ID Not Found.');
            header('location:'.url('?page=spk.setting&subPage=kriteria'));
        }

        $nilaiBobotSubKriteria->generateDefaultBobot($kriteria_id);

        $subKriterias = $subKriteria->select('WHERE kriteria_id='.$kriteria_id);
    }else{
        $session->setSession('warning', 'URL Error');
        header('location:'.url('?page=spk.setting&subPage=kriteria'));
    }
?>
<!-- Breadcrumb -->
<ol class="breadcrumb bg-white">
    <li class="breadcrumb-item"><a href="<?=url('?page=spk.setting')?>">SPK Setting</a></li>
    <li class="breadcrumb-item"><a href="<?=url('?page=spk.setting&subPage=kriteria')?>">Kriteria</a></li>
    <li class="breadcrumb-item"><a href="<?=url('?page=sub.kriteria.table&subPage=kriteria&parent_id='.$kriteria_id)?>"><?=$kriteria['kriteria']?></a></li>
    <li class="breadcrumb-item active">Nilai Poin</li>
</ol>
<!-- Content -->
<div class="card">
    <div class="card-body">
        <div class="row p-3">
            <div class="col-12">
                <h4>Form Perbandingan Berpasangan</h4>
                <form class="row" action="?page=sub.kriteria.perbandingan&subPage=kriteria&parent_id=<?=$kriteria_id?>" method="post">
                    <input type="hidden" name="model" value="nilaiBobotSubKriteria"/>
                    <input type="hidden" name="edit" value="true"/>
                    <input type="hidden" name="kriteria_id" value="<?=$kriteria_id?>"/>
                    <div class="col-2">
                        <select name="sub_kriteria_id1" id="sub_kriteria_id1" class="form-control input-sub_kriteria_id">
                        <?php
                            foreach($subKriterias as $subKriteria)
                            {
                        ?>
                            <option value="<?=$subKriteria['sub_kriteria_id']?>"><?=$subKriteria['sub_kriteria']?></option>
                        <?php
                            }
                        ?>
                        </select>
                    </div>

                    <div class="col-6 text-center">
                        <?php
                            $key = [
                                9,8,7,6,5,4,3,2,1,2,3,4,5,6,7,8,9
                            ];

                            $value = [
                                9,
                                8,
                                7,
                                6,
                                5,
                                4,
                                3,
                                2,
                                1,
                                1/2,
                                1/3,
                                1/4,
                                1/5,
                                1/6,
                                1/7,
                                1/8,
                                1/9,
                            ];
                        ?>
                        <div class="btn-group">
                        <?php
                            foreach($value as $index => $bobot)
                            {
                        ?>
                            <button type="button" class="btn btn-outline-primary btn-sm btn-insert-bobot" id="btn-bobot<?=$index?>" data-value="<?=$bobot?>"><?=$key[$index]?></button>
                        <?php
                            }
                        ?>
                        </div>
                        <input type="hidden" name="bobot" id="bobot">
                    </div>

                    <div class="col-2">
                        <select name="sub_kriteria_id2" id="sub_kriteria_id2" class="form-control input-sub_kriteria_id">
                        <?php
                            foreach($subKriterias as $subKriteria)
                            {
                        ?>
                            <option value="<?=$subKriteria['sub_kriteria_id']?>"><?=$subKriteria['sub_kriteria']?></option>
                        <?php
                            }
                        ?>
                        </select>
                    </div>
                    <div class="col-2">
                        <button type="submit" id="btn-submit-bobot" class="btn btn-success">Edit</button>
                    </div>
                </form>
            </div>
        </div>
        <div class="row p-3">
            <div class="col-12">
                <h4>Table Perbandingan Berpasangan</h4>
                <table class="table table-bordered table-hover">
                    <thead>
                        <tr>
                            <th><?=$kriteria['kriteria']?></th>
                        <?php
                            foreach($subKriterias as $subKriteria)
                            {
                        ?>
                            <th><?=$subKriteria['sub_kriteria']?></th>
                        <?php
                            }
                        ?>
                        </tr>
                    </thead>
                    <tbody>
                    <?php
                        foreach($subKriterias as $subKriteria1)
                            {
                        ?>
                            <tr>
                                <th><?=$subKriteria1['sub_kriteria']?></th>
                                <?php
                                    foreach($subKriterias as $subKriteria2)
                                    {
                                ?>
                                    <td class="bobot-nilai" id="bobot-<?=$subKriteria1['sub_kriteria_id']?>-<?=$subKriteria2['sub_kriteria_id']?>"><?=$nilai = number_format($nilaiBobotSubKriteria->selectBobot($subKriteria1['sub_kriteria_id'], $subKriteria2['sub_kriteria_id']),2)?></td>
                                <?php
                                    }
                                ?>
                            </tr>
                        <?php
                            }
                        ?>
                        <tr>
                            <th>Jumlah</th>
                            <?php
                                foreach($subKriterias as $subKriteria)
                                {
                            ?>
                                <td><?=number_format($nilaiBobotSubKriteria->selectJumlah($subKriteria['sub_kriteria_id']),2)?></td>
                            <?php
                                }
                            ?>
                        </tr>
                    </tbody>
                </table>
                <form action="?page=sub.kriteria.perbandingan&subPage=kriteria&parent_id=<?=$kriteria_id?>" method="post">
                    <input type="hidden" name="hitung" value="true"/>            
                    <button type="submit" class="btn btn-info">Hitung</button>
                </form>
            </div>
        </div>

    <?php
        if(isset($_POST['hitung']))
        {
    ?>
        <div class="row p-3">
            <div class="col-12">
                <h4>Matriks Nilai Sub Kriteria (Normalisasi)</h4>
                <table class="table table-bordered table-hover">
                    <thead>
                        <tr>
                            <th><?=$kriteria['kriteria']?></th>
                        <?php
                            foreach($subKriterias as $subKriteria)
                            {
                        ?>
                            <th><?=$subKriteria['sub_kriteria']?></th>
                        <?php
                            }
                        ?>
                            <th>Jumlah</th>
                            <th>Prioritas</th>
                            <th>Sub Prioritas</th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php
                        foreach($subKriterias as $subKriteria1)
                            {
                        ?>
                            <tr>
                                <th><?=$subKriteria1['sub_kriteria']?></th>
                                <?php
                                    foreach($subKriterias as $subKriteria2)
                                    {
                                ?>
                                    <td><?=$nilaiBobotSubKriteria->selectNormalisasi($subKriteria1['sub_kriteria_id'], $subKriteria2['sub_kriteria_id'])?></td>
                                <?php  
                                    }
                                ?>
                                <td><?=$nilaiBobotSubKriteria->selectJumlahNormalisasi($subKriteria1['sub_kriteria_id'], $subKriterias)?></td>
                                <td><?=$nilaiBobotSubKriteria->selectPrioritas($subKriteria1['sub_kriteria_id'], $subKriterias)?></td>
                                <td><?=$nilaiBobotSubKriteria->selectPrioritasSubKriteria($subKriteria1['sub_kriteria_id'], $subKriterias)?></td>
                            </tr>
                        <?php
                            }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="row p-3">
            <div class="col-12">
                <h4>Matriks Penjumlahan Setiap Baris</h4>
                <table class="table table-bordered table-hover">
                    <thead>
                        <tr>
                            <th><?=$kriteria['kriteria']?></th>
                        <?php
                            foreach($subKriterias as $subKriteria)
                            {
                        ?>
                            <th><?=$subKriteria['sub_kriteria']?></th>
                        <?php
                            }
                        ?>
                            <th>Jumlah</th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php
                        foreach($subKriterias as $subKriteria1)
                            {
                        ?>
                            <tr>
                                <th><?=$subKriteria1['sub_kriteria']?></th>
                                <?php
                                    foreach($subKriterias as $subKriteria2)
                                    {
                                ?>
                                    <td><?=$nilaiBobotSubKriteria->selectMatriksPenjumlahan($subKriteria1['sub_kriteria_id'], $subKriteria2['sub_kriteria_id'], $subKriterias)?></td>
                                <?php  
                                    }
                                ?>
                                <td><?=$nilaiBobotSubKriteria->selectJumlahPenjumlahan($subKriteria1['sub_kriteria_id'], $subKriterias)?></td>
                            </tr>
                        <?php
                            }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="row p-3">
            <div class="col-12">
                <h4>Perhitungan Rasio Konsistensi</h4>
                <table class="table table-bordered table-hover">
                    <thead>
                        <tr>
                            <th><?=$kriteria['kriteria']?></th>
                            <th>Jumlah</th>
                            <th>Prioritas</th>
                            <th>Hasil</th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php
                        foreach($subKriterias as $subKriteria1)
                            {
                        ?>
                            <tr>
                                <th><?=$subKriteria1['sub_kriteria']?></th>
                                <td><?=$nilaiBobotSubKriteria->selectJumlahPenjumlahan($subKriteria1['sub_kriteria_id'], $subKriterias)?></td>
                                <td><?=$nilaiBobotSubKriteria->selectPrioritas($subKriteria1['sub_kriteria_id'], $subKriterias)?></td>
                                <td><?=$nilaiBobotSubKriteria->selectHasil($subKriteria1['sub_kriteria_id'], $subKriterias)?></td>
                            </tr>
                        <?php
                            }
                        ?>
                        <tr>
                            <th colspan="3" class="text-right">Jumlah</th>
                            <td><?=$nilaiBobotSubKriteria->selectJumlahHasil($subKriterias)?></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="row p-3">
            <div class="offset-3 col-6">
                <h4>Konsistensi</h4>
                <table class="table table-bordered table-striped table-hover">
                    <tr>
                        <th>&lambda; Maks</th>
                        <td><?=$nilaiBobotSubKriteria->selectLambdaMaks($subKriterias)?></td>
                    </tr>
                    <tr>
                        <th>CI</th>
                        <td><?=$nilaiBobotSubKriteria->selectCI($subKriterias)?></td>
                    </tr>
                    <tr>
                        <th>CR</th>
                        <td><?=$nilaiBobotSubKriteria->selectCR($subKriterias)?></td>
                    </tr>
                    <tr class="text-light <?=($nilaiBobotSubKriteria->selectKonsistensi($subKriterias)) ? 'bg-success': 'bg-danger'?>">
                        <th colspan="2" class="text-center"><?=($nilaiBobotSubKriteria->selectKonsistensi($subKriterias)) ? 'KONSISTEN': 'TIDAK KONSISTEN'?></th>
                    </tr>
                </table>
            </div>
            <div class="offset-2 col-8">
                <?php
                    if($nilaiBobotSubKriteria->selectKonsistensi($subKriterias))
                    {
                ?>
                    <form action="?page=sub.kriteria.table&subPage=kriteria&parent_id=<?=$kriteria_id?>" method="post" class="my-3">
                        <input type="hidden" name="model" value="nilaiBobotSubKriteria"/>
                        <input type="hidden" name="save" value="true"/>
                        <input type="hidden" name="kriteria_id" value="<?=$kriteria_id?>"/>
                        <button type="submit" class="btn btn-primary">Simpan</button>
                    </form>
                <?php
                    }else{
                ?>
                    <div class="alert alert-warning" role="alert">
                        Hasil perhitungan Tidak Konsisten. Harap Ulangi Perbandingan!
                    </div>
                <?php
                    }
                ?>
            </div>
        </div>
    <?php
        }
    ?>
    </div>
</div>

<script>
    function changeButtonBobotActive(){
        var sub_kriteria_id1 = $('#sub_kriteria_id1').val();
        var sub_kriteria_id2 = $('#sub_kriteria_id2').val();

        if(sub_kriteria_id1 == sub_kriteria_id2){
            $('#btn-submit-bobot').prop("disabled", true);
            $('.btn-insert-bobot').prop("disabled", true);
        }else{
            $('#btn-submit-bobot').prop("disabled", false);
            $('.btn-insert-bobot').prop("disabled", false);
        }
        $('.bobot-nilai').removeClass('bg-warning');
        $('#bobot-'+sub_kriteria_id1+'-'+sub_kriteria_id2).addClass('bg-warning');
        var bobot = $('#bobot-'+sub_kriteria_id1+'-'+sub_kriteria_id2).html();
        var index = 0;
        switch (bobot) {
            case '0.11':
                index = 16;
                break;
            case '0.13':
                index = 15;
                break;
            case '0.14':
                index = 14;
                break;
            case '0.17':
                index = 13;
                break;
            case '0.20':
                index = 12;
                break;
            case '0.25':
                index = 11;
                break;
            case '0.33':
                index = 10;
                break;
            case '0.50':
                index = 9;
                break;
            case '1.00':
                index = 8;
                break;
            case '2.00':
                index = 7;
                break;
            case '3.00':
                index = 6;
                break;
            case '4.00':
                index = 5;
                break;
            case '5.00':
                index = 4;
                break;
            case '6.00':
                index = 3;
                break;
            case '7.00':
                index = 2;
                break;
            case '8.00':
                index = 1;
                break;
            case '9.00':
                index = 0;
                break;
            default:
                break;
        }
        $('.btn-insert-bobot').removeClass('btn-primary').addClass('btn-outline-primary');
        $('#btn-bobot'+index).removeClass('btn-outline-primary').addClass('btn-primary');
        var bobot = $('#btn-bobot'+index).data('value');
        $('#bobot').val(bobot);
    }

    $(document).ready(function(){
        changeButtonBobotActive();

        // Event Onchange SubKriteria
        $('.input-sub_kriteria_id').on('change', function(){
            
            changeButtonBobotActive();
        })

        // Event Click Button Perbandingan
        $('.btn-insert-bobot').click(function(){
            $('.btn-insert-bobot').removeClass('btn-primary').addClass('btn-outline-primary');
            $(this).removeClass('btn-outline-primary').addClass('btn-primary');
            var bobot = $(this).data('value');
            $('#bobot').val(bobot);
        })
    })
</script>
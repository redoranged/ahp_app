<?php
    $kriteria = new Kriteria();

    $kriterias = $kriteria->select();
?>
<!-- Breadcrumb -->
<ol class="breadcrumb bg-white">
    <li class="breadcrumb-item"><a href="<?=url('?page=spk.setting')?>">SPK Setting</a></li>
    <li class="breadcrumb-item active">Kriteria</li>
</ol>
<!-- Content -->
<div class="card">
    <div class="card-body">
        <?php
        if(count($kriterias) < 3)
        {
        ?>
        <div class="row p-3">
            <div class="col-12">
                <div class="alert alert-warning alert-dismissible fade show" role="alert">
                    <strong>Warning!</strong> Data Kriteria kurang dari 3. Harap tambah terlebih dahulu!
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            </div>
        </div>
        <?php
        }
        ?>
        <div class="row">
            <div class="col-lg-6 col-sm-12">
                <div class="btn-group" role="group">
                    <a href="<?=url('?page=kriteria.form&subPage=kriteria')?>" class="btn btn-sm btn-outline-success">Tambah Kriteria</a>
                    <?php
                        if(count($kriterias) > 2)
                        {
                    ?>
                        <a href="<?=url('?page=kriteria.perbandingan&subPage=kriteria')?>" class="btn btn-sm btn-outline-info">Nilai Poin Kriteria</a>
                    <?php
                        }
                    ?>
                </div>
            </div>
        </div>
        <div class="row mt-3">
            <div class="col-12">
                <table class="table table-bordered table-hover" id="tableKriteria">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Kode Kriteria</th>
                            <th>Kriteria</th>
                            <th>Nilai Prioritas</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        if(count($kriterias) > 0)
                        {
                            foreach($kriterias as $key => $kriteria)
                            {
                        ?>
                        <tr>
                            <td><?=$key+1?></td>
                            <td><?=$kriteria['kode_kriteria']?></td>
                            <td><?=$kriteria['kriteria']?></td>
                            <td><?=$kriteria['nilai_prioritas']?></td>
                            <td>
                                <form action="?page=spk.setting&subPage=kriteria" method="POST">
                                    <input type="hidden" name="model" value="kriteria"/>
                                    <input type="hidden" name="kriteria_id" value="<?=$kriteria['kriteria_id']?>"/>
                                    <div class="btn-group" role="group">
                                        <a href="<?=url('?page=kriteria.form&subPage=kriteria&id='.$kriteria['kriteria_id'])?>" class="btn btn-sm btn-info">Edit</a>
                                        <button type="submit" name="delete" class="btn btn-sm btn-danger" onclick="return confirm('Hapus Kriteria?')">Hapus</button>
                                        <a href="<?=url('?page=sub.kriteria.table&subPage=kriteria&parent_id='.$kriteria['kriteria_id'])?>" class="btn btn-sm btn-warning">Sub Kriteria</a>
                                    </div>
                                </form>
                            </td>
                        </tr>
                        <?php
                            }
                        }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function(){
        $('#tableKriteria').DataTable();
    })
</script>
<div class="jumbotron p-3 my-3 jumbotron-fluid text-center bg-info text-light">
  <div class="container">
    <h1 class="display-4">Sistem Pendukung Keputusan</h1>
    <p class="lead">Sistem ini memudahkan bagi konsumen untuk memilih jenis kain sesuai dengan kriteria yang diinginkan.
                <br>Dimana konsumen hanya perlu memberikan bobot kepada setiap kriteria, pemberian bobot yang nantinya akan memberikan rekomendasi kain yang sesuai.</p>
    <p class="lead">
      <a href="<?=url('?page=spk.form')?>" class="btn btn-lg btn-success">Lanjut</a>
    </p>
  </div>
</div>
<style>
.logohome {
  width: 500px;
}
</style>

<div class="jumbotron p-3 my-3 jumbotron-fluid bg-dark text-light text-center">
  <div class="container">
    <img class="logohome d-block ml-auto mr-auto img-fluid" src="<?=url('/image/logo_brother.png')?>">
    <p class="lead">BROTHER COMPANY berpengalaman sejak tahun 2015 menerima order / memproduksi berbagai jenis pakaian seragam mulai dari yang ekonomis sampai yang exclusive, dengan atribut sablon dan bordir komputer, seperti:
    <br><br>Kemeja Kerja, Kemeja Promosi, Kaos T Shirt, Kaos Promosi, Kaos Kerah, Polo Shirt, Jaket Kerja, Jaket olahraga, Training Suit (Training Spak), Rompi, Seragam Lapangan,  Wearpack (Coverall/Overall), Celana Kerja, Celana Olahraga, Sweater, Topi, Tas, Handuk dan Payung Promosi.
    <br><br>Keseluruhan proses produksi ditangani oleh tenaga yang terlatih dengan kontrol kerja yang maksimal, karena produk yang baik dihasilkan melalui sistem kerja yang teratur di setiap lini produksi. Kami mengutamakan kepuasan konsumen dan tidak semata-mata mengejar profit.
            Kelanggengan hubungan kerja sama menjadi sasaran kami.</p>
  </div>
</div>
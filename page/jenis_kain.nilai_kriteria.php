<?php
    // Form Setup
    $id = '';
    $jenis_kain = new JenisKain();
    $jenisKainNilaiKriteria = new JenisKainNilaiKriteria();
    if(isset($_GET['id']))
    {
        $id = $_GET['id'];
        $jenis_kain = $jenis_kain->find($id);
        if(!$jenis_kain)
        {
            $session->setSession('warning', 'Jenis Kain ID Not Found.');
            header('location:'.url('?page=spk.setting&subPage=jenis_kain'));
        }
        $jenisKainNilaiKriteria->generateDefaultNilaiKriteria($id);
        $jenisKainNilaiKriteria = $jenisKainNilaiKriteria->select('WHERE jenis_kain_id='.$id);
    }
?>
<!-- Breadcrumb -->
<ol class="breadcrumb bg-white">
    <li class="breadcrumb-item"><a href="<?=url('?page=spk.setting')?>">SPK Setting</a></li>
    <li class="breadcrumb-item"><a href="<?=url('?page=spk.setting&subPage=jenis_kain')?>">Jenis Kain</a></li>
    <li class="breadcrumb-item active"><?=$jenis_kain['jenis_kain']?></li>
    <li class="breadcrumb-item active">Nilai Kriteria</li>
</ol>
<!-- Content -->
<div class="card">
    <div class="card-body">
        <div class="row">
            <div class="col-12">
                <form action="?page=spk.setting&subPage=jenis_kain" method="POST">
                    <input type="hidden" name="model" value="jenisKainNilaiKriteria"/>
                    <input type="hidden" name="add" value="true"/>
                    <div class="form-group row" style="display:none;">
                        <label class="col-sm-2 col-form-label" for="jenis_kain_id">Jenis Kain ID</label>
                        <div class="col-sm-10">
                            <input type="text" name="jenis_kain_id" value="<?=$id?>" id="jenis_kain_id" placeholder="Jenis Kain ID" class="form-control" readonly/>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label" for="jenis_kain">Jenis Kain</label>
                        <div class="col-sm-10">
                            <input type="text" name="jenis_kain" value="<?=$jenis_kain['jenis_kain']?>" id="jenis_kain" placeholder="Jenis Kain" class="form-control" readonly/>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label" for="keterangan">Keterangan</label>
                        <div class="col-sm-10">
                            <textarea name="keterangan" id="keterangan" rows="3" class="form-control" readonly><?=$jenis_kain['keterangan']?></textarea>
                        </div>
                    </div>
                    <div class="container-fluid">
                        <div class="col-12">
                            <h4>Nilai Kriteria</h4>
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th>Kriteria</th>
                                        <th>Sub Kriteria</th>
                                    </tr>
                                </thead>
                                <tbody>
        
                                    <?php
                                        foreach($jenisKainNilaiKriteria as $index => $kriteria)
                                        {
                                    ?>
                                    <tr>
                                        <td>
                                            <?=$kriteria['kriteria']['kriteria']?>
                                            <input type="hidden" name="kriteria_id[<?=$index?>]" id="kriteria_id[<?=$index?>]" value="<?=$kriteria['kriteria_id']?>">
                                        </td>
                                        <td>
                                            <select name="sub_kriteria_id[<?=$index?>]" id="sub_kriteria_id[<?=$index?>]" class="form-control" required>
                                            <?php
                                                foreach($kriteria['kriteria']['sub_kriteria'] as $sub_kriteria)
                                                {
                                            ?>
                                                <option value="<?=$sub_kriteria['sub_kriteria_id']?>" <?=($sub_kriteria['sub_kriteria_id'] == $kriteria['sub_kriteria_id'])?'selected':''?>><?=$sub_kriteria['sub_kriteria']?></option>
                                            <?php
                                                }
                                            ?>
                                            </select>
                                        </td>
                                    </tr>
                                    <?php
                                        }
                                    ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <button type="submit" class="btn btn-success"><?='Simpan'?></button>
                    <a href="<?=url('?page=spk.setting&subPage=jenis_kain')?>" class="btn btn-danger">Batal</a>
                </form>
            </div>
        </div>
    </div>
</div>
<form action="<?=url('/?page=login&&login=true')?>" method="POST" class="form-signin">
    </br>
    </br>
    </br>
    <div class="form-label-group">
        <input type="username" name="username" id="inputUsername" class="form-control" placeholder="Username" required autofocus>
        <label for="inputUsernmae">Username</label>
    </div>

    <div class="form-label-group">
        <input type="password" name="password" id="inputPassword" class="form-control" placeholder="Password" required>
        <label for="inputPassword">Password</label>
    </div>

    <div class="checkbox mb-3">
        <label>
        <input type="checkbox" value="remember-me"> Remember me
        </label>
    </div>
    <button class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>
    <p class="mt-5 mb-3 text-muted text-center">&copy;<?=date('Y')?></p>
</form>
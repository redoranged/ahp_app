<?php
    $user = new User();

    $admins = $user->select();
?>
<!-- Breadcrumb -->
<ol class="breadcrumb bg-white">
    <li class="breadcrumb-item"><a href="<?=url('/')?>">Home</a></li>
    <li class="breadcrumb-item active">Admin</li>
</ol>
<!-- Content -->
<div class="card">
    <div class="card-body">
        <div class="row">
            <div class="col-lg-6 col-sm-12">
                <div class="btn-group" role="group">
                    <a href="<?=url('?page=user.form')?>" class="btn btn-sm btn-outline-success">Tambah Admin</a>
                </div>
            </div>
        </div>
        <div class="row mt-3">
            <div class="col-12">
                <table class="table table-bordered table-hover" id="tableUser">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Name</th>
                            <th>Username</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        if(count($admins) > 0)
                        {
                            foreach($admins as $key => $user)
                            {
                        ?>
                        <tr>
                            <td><?=$key+1?></td>
                            <td><?=$user['name']?></td>
                            <td><?=$user['username']?></td>
                            <td>
                                <form action="?page=user" method="POST">
                                    <input type="hidden" name="model" value="user"/>
                                    <input type="hidden" name="user_id" value="<?=$user['user_id']?>"/>
                                    <div class="btn-group" role="group">
                                        <a href="<?=url('?page=user.form&id='.$user['user_id'])?>" class="btn btn-sm btn-info">Edit</a>
                                    <?php
                                        if($session->user_id != $user['user_id'])
                                        {
                                    ?>
                                        <button type="submit" name="delete" class="btn btn-sm btn-danger" onclick="return confirm('Hapus User?')">Hapus</button>
                                    <?php
                                        }
                                    ?>
                                    </div>
                                </form>
                            </td>
                        </tr>
                        <?php
                            }
                        }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function(){
        $('#tableUser').DataTable();
    })
</script>
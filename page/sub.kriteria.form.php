<?php
    // Form Setup
    $formType = 'add';
    $id = '';
    $kriteria_id = '';
    $kriteria = new Kriteria();
    $subKriteria = new SubKriteria();
    if(isset($_GET['parent_id']))
    {
        $kriteria_id = $_GET['parent_id'];
        $kriteria = $kriteria->find($kriteria_id);
        if(!$kriteria)
        {
            $session->setSession('warning', 'Kriteria ID Not Found.');
            header('location:'.url('?page=spk.setting&subPage=kriteria'));
        }

        if(isset($_GET['id']))
        {
            $formType = 'edit';
            $id = $_GET['id'];
            $subKriteria = $subKriteria->find($id);
            if(!$subKriteria)
            {
                $session->setSession('warning', 'Sub Kriteria ID Not Found.');
                header('location:'.url('?page=sub.kriteria.table&subPage=kriteria&parent_id='.$kriteria['kriteria_id']));
            }
        }
    }else{
        $session->setSession('warning', 'URL Error');
        header('location:'.url('?page=spk.setting&subPage=kriteria'));
    }
?>
<!-- Breadcrumb -->
<ol class="breadcrumb bg-white">
    <li class="breadcrumb-item"><a href="<?=url('?page=spk.setting')?>">SPK Setting</a></li>
    <li class="breadcrumb-item"><a href="<?=url('?page=spk.setting&subPage=kriteria')?>">Kriteria</a></li>
    <li class="breadcrumb-item"><a href="<?=url('?page=sub.kriteria.table&subPage=kriteria&parent_id='.$kriteria_id)?>"><?=$kriteria['kriteria']?></a></li>
    <li class="breadcrumb-item active"><?=($formType=='add')?'New':$subKriteria['sub_kriteria']?></li>
</ol>
<!-- Content -->
<div class="card">
    <div class="card-body">
        <div class="row">
            <div class="col-12">
                <form action="?page=sub.kriteria.table&subPage=kriteria&parent_id=<?=$kriteria_id?>" method="POST">
                    <input type="hidden" name="model" value="subKriteria"/>
                    <input type="hidden" name="<?=$formType?>" value="true"/>
                    <div class="form-group row" style="display:none;">
                        <label class="col-sm-2 col-form-label" for="sub_kriteria_id">Sub Kriteria ID</label>
                        <div class="col-sm-10">
                            <input type="text" name="sub_kriteria_id" value="<?=$id?>" id="sub_kriteria_id" placeholder="Sub Kriteria ID" class="form-control" readonly/>
                        </div>
                    </div>
                    <div class="form-group row" style="display:none;">
                        <label class="col-sm-2 col-form-label" for="kriteria_id">Kriteria ID</label>
                        <div class="col-sm-10">
                            <input type="text" name="kriteria_id" value="<?=$kriteria_id?>" id="kriteria_id" placeholder="Kriteria ID" class="form-control" readonly/>
                        </div>
                    </div>
                    <div class="form-group row" style="display:none;">
                        <label class="col-sm-2 col-form-label" for="poin">Poin</label>
                        <div class="col-sm-10">
                            <input type="number" name="poin" value="<?=($formType=='edit')?$subKriteria['poin']:''?>" id="poin" placeholder="Poin" class="form-control"/>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label" for="sub_kriteria">Sub Kriteria</label>
                        <div class="col-sm-10">
                            <input type="text" name="sub_kriteria" value="<?=($formType=='edit')?$subKriteria['sub_kriteria']:''?>" id="sub_kriteria" placeholder="Sub Kriteria" class="form-control" autofocus required/>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label" for="keterangan">Keterangan</label>
                        <div class="col-sm-10">
                            <textarea name="keterangan" id="keterangan" rows="3" class="form-control"><?=($formType=='edit')?$subKriteria['keterangan']:''?></textarea>
                        </div>
                    </div>
                    <button type="submit" class="btn btn-success"><?=($formType=='add')?'Tambah':'Edit'?></button>
                    <a href="<?=url('?page=sub.kriteria.table&subPage=kriteria&parent_id='.$kriteria_id)?>" class="btn btn-danger">Batal</a>
                </form>
            </div>
        </div>
    </div>
</div>
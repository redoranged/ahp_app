<?php
    // Form Setup
    $kriteria = new Kriteria();
    $nilaiBobotKriteria = new NilaiBobotKriteria();

    // $nilaiBobotKriteria->generateDefaultBobot();
    $bobotKriteria = $nilaiBobotKriteria->select();
    $jenisKain = new JenisKain();
    $jenisKainNilaiKriteria = new JenisKainNilaiKriteria();

    $jenisKains = $jenisKain->select();
    $jenisKainNilaiKriterias = $jenisKainNilaiKriteria->select();

    $kriterias = $kriteria->select();
?>
<!-- Breadcrumb -->
<ol class="breadcrumb bg-white">
    <li class="breadcrumb-item"><a href="<?=url('?page=spk')?>">SPK</a></li>
    <li class="breadcrumb-item active">Form</li>
</ol>
<!-- Content -->
<div class="card">
    <div class="card-body">
        <?php
        if(count($kriterias) < 3)
        {
        ?>
        <div class="row p-3">
            <div class="col-12">
                <div class="alert alert-warning alert-dismissible fade show" role="alert">
                    <strong>Warning!</strong> Hubungi admin untuk menambahkan data kriteria!
                </div>
            </div>
        </div>
        <?php
        }else{
            if(count($bobotKriteria) > 0 && count($jenisKains) > 0 && count($jenisKainNilaiKriterias) > 0)
            {
        ?>
        <div class="row p-3">
            <div class="col-12">
                <h4>Form Perbandingan Berpasangan</h4>
                <form class="row" action="?page=spk.form" method="post">
                    <input type="hidden" name="model" value="nilaiBobotKriteria"/>
                    <input type="hidden" name="edit" value="true"/>
                    <div class="col-2">
                        <select name="kriteria_id1" id="kriteria_id1" class="form-control input-kriteria_id">
                        <?php
                            foreach($kriterias as $kriteria)
                            {
                        ?>
                            <option value="<?=$kriteria['kriteria_id']?>"><?=$kriteria['kriteria']?></option>
                        <?php
                            }
                        ?>
                        </select>
                    </div>

                    <div class="col-6 text-center">
                        <?php
                            $value = [
                                "Sama penting dengan",
                                "Mendekati sedikit lebih penting dari",
                                "Sedikit lebih penting dari",
                                "Mendekati lebih penting dari",
                                "Lebih penting dari",
                                "Mendekati sangat penting dari",
                                "Sangat penting dari",
                                "Mendekati mutlak dari",
                                "Mutlak sangat penting dari"
                            ];
                        ?>
                        <select name="bobot" id="bobot" class="form-control">
                        <?php
                            foreach($value as $bobot => $deskripsi)
                            {
                        ?>
                            <option value="<?=$bobot+1?>" id="bobot-nilai-<?=$bobot+1?>"><?=($bobot+1).". ".$deskripsi?></option>
                        <?php
                            }
                        ?>
                        </select>
                    </div>

                    <div class="col-2">
                        <select name="kriteria_id2" id="kriteria_id2" class="form-control input-kriteria_id">
                        <?php
                            foreach($kriterias as $kriteria)
                            {
                        ?>
                            <option value="<?=$kriteria['kriteria_id']?>"><?=$kriteria['kriteria']?></option>
                        <?php
                            }
                        ?>
                        </select>
                    </div>
                    <div class="col-2">
                        <button type="submit" id="btn-submit-bobot" class="btn btn-success">Edit</button>
                    </div>
                </form>
            </div>
        </div>
        <div class="row p-3">
            <div class="col-12">
                <h4>Table Perbandingan Berpasangan</h4>
                <table class="table table-bordered table-hover">
                    <thead>
                        <tr>
                            <th>Kriteria</th>
                        <?php
                            foreach($kriterias as $kriteria)
                            {
                        ?>
                            <th><?=$kriteria['kriteria']?></th>
                        <?php
                            }
                        ?>
                        </tr>
                    </thead>
                    <tbody>
                    <?php
                        foreach($kriterias as $kriteria1)
                            {
                        ?>
                            <tr>
                                <th><?=$kriteria1['kriteria']?></th>
                                <?php
                                    foreach($kriterias as $kriteria2)
                                    {
                                ?>
                                    <td class="bobot-nilai" id="bobot-<?=$kriteria1['kriteria_id']?>-<?=$kriteria2['kriteria_id']?>"><?=$nilai = number_format($nilaiBobotKriteria->selectBobot($kriteria1['kriteria_id'], $kriteria2['kriteria_id']),2)?></td>
                                <?php
                                    }
                                ?>
                            </tr>
                        <?php
                            }
                        ?>
                        <tr>
                            <th>Jumlah</th>
                            <?php
                                foreach($kriterias as $kriteria)
                                {
                            ?>
                                <td><?=number_format($nilaiBobotKriteria->selectJumlah($kriteria['kriteria_id']),2)?></td>
                            <?php
                                }
                            ?>
                        </tr>
                    </tbody>
                </table>
                <form action="?page=spk.rekomendasi" method="post">
                    <input type="hidden" name="hitung" value="true"/>            
                    <button type="submit" class="btn btn-info">Hitung</button>
                </form>
            </div>
        </div>
        <?php
            }else{
        ?>
            <div class="row p-3">
                <div class="col-12">
                    <div class="alert alert-warning alert-dismissible fade show" role="alert">
                        <strong>Warning!</strong> Hubungi admin untuk memberikan bobot, Jenis Kain & Nilai Kriteria Kain!
                    </div>
                </div>
            </div>
            <?php
            }
        ?>
        <?php
        }
        ?>
    </div>
</div>

<script>
    function changeButtonBobotActive(){
        var kriteria_id1 = $('#kriteria_id1').val();
        var kriteria_id2 = $('#kriteria_id2').val();

        if(kriteria_id1 == kriteria_id2){
            $('#btn-submit-bobot').prop("disabled", true);
        }else{
            $('#btn-submit-bobot').prop("disabled", false);
        }
        $('.bobot-nilai').removeClass('bg-warning');
        $('#bobot-'+kriteria_id1+'-'+kriteria_id2).addClass('bg-warning');
    }

    $(document).ready(function(){
        changeButtonBobotActive();

        // Event Onchange Kriteria
        $('.input-kriteria_id').on('change', function(){
            
            changeButtonBobotActive();
        })
    })
</script>
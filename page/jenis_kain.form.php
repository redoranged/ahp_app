<?php
    // Form Setup
    $formType = 'add';
    $id = '';
    $jenis_kain = new JenisKain();
    if(isset($_GET['id']))
    {
        $formType = 'edit';
        $id = $_GET['id'];
        $jenis_kain = $jenis_kain->find($id);
        if(!$jenis_kain)
        {
            $session->setSession('warning', 'Jenis Kain ID Not Found.');
            header('location:'.url('?page=spk.setting&subPage=jenis_kain'));
        }
    }
?>
<!-- Breadcrumb -->
<ol class="breadcrumb bg-white">
    <li class="breadcrumb-item"><a href="<?=url('?page=spk.setting')?>">SPK Setting</a></li>
    <li class="breadcrumb-item"><a href="<?=url('?page=spk.setting&subPage=jenis_kain')?>">Jenis Kain</a></li>
    <li class="breadcrumb-item active"><?=($formType=='add')?'New':$jenis_kain['jenis_kain']?></li>
</ol>
<!-- Content -->
<div class="card">
    <div class="card-body">
        <div class="row">
            <div class="col-12">
                <form action="?page=spk.setting&subPage=jenis_kain" method="POST">
                    <input type="hidden" name="model" value="jenisKain"/>
                    <input type="hidden" name="<?=$formType?>" value="true"/>
                    <div class="form-group row" style="display:none;">
                        <label class="col-sm-2 col-form-label" for="jenis_kain_id">Jenis Kain ID</label>
                        <div class="col-sm-10">
                            <input type="text" name="jenis_kain_id" value="<?=$id?>" id="jenis_kain_id" placeholder="Jenis Kain ID" class="form-control" readonly/>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label" for="jenis_kain">Jenis Kain</label>
                        <div class="col-sm-10">
                            <input type="text" name="jenis_kain" value="<?=($formType=='edit')?$jenis_kain['jenis_kain']:''?>" id="jenis_kain" placeholder="Jenis Kain" class="form-control" autofocus required/>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label" for="keterangan">Keterangan</label>
                        <div class="col-sm-10">
                            <textarea name="keterangan" id="keterangan" rows="3" class="form-control"><?=($formType=='edit')?$jenis_kain['keterangan']:''?></textarea>
                        </div>
                    </div>
                    <button type="submit" class="btn btn-success"><?=($formType=='add')?'Tambah':'Edit'?></button>
                    <a href="<?=url('?page=spk.setting&subPage=jenis_kain')?>" class="btn btn-danger">Batal</a>
                </form>
            </div>
        </div>
    </div>
</div>
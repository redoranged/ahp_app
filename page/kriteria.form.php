<?php
    // Form Setup
    $formType = 'add';
    $id = '';
    $kriteria = new Kriteria();
    if(isset($_GET['id']))
    {
        $formType = 'edit';
        $id = $_GET['id'];
        $kriteria = $kriteria->find($id);
        if(!$kriteria)
        {
            $session->setSession('warning', 'Kriteria ID Not Found.');
            header('location:'.url('?page=spk.setting&subPage=kriteria'));
        }
    }
?>
<!-- Breadcrumb -->
<ol class="breadcrumb bg-white">
    <li class="breadcrumb-item"><a href="<?=url('?page=spk.setting')?>">SPK Setting</a></li>
    <li class="breadcrumb-item"><a href="<?=url('?page=spk.setting&subPage=kriteria')?>">Kriteria</a></li>
    <li class="breadcrumb-item active"><?=($formType=='add')?'New':$kriteria['kriteria']?></li>
</ol>
<!-- Content -->
<div class="card">
    <div class="card-body">
        <div class="row">
            <div class="col-12">
                <form action="?page=spk.setting&subPage=kriteria" method="POST">
                    <input type="hidden" name="model" value="kriteria"/>
                    <input type="hidden" name="<?=$formType?>" value="true"/>
                    <div class="form-group row" style="display:none;">
                        <label class="col-sm-2 col-form-label" for="kriteria_id">Kriteria ID</label>
                        <div class="col-sm-10">
                            <input type="text" name="kriteria_id" value="<?=$id?>" id="kriteria_id" placeholder="Kriteria ID" class="form-control" readonly/>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label" for="kode_kriteria">Kode Kriteria</label>
                        <div class="col-sm-10">
                            <input type="text" name="kode_kriteria" value="<?=($formType=='edit')?$kriteria['kode_kriteria']:''?>" id="kode_kriteria" placeholder="Kode Kriteria" class="form-control" autofocus required/>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label" for="kriteria">Kriteria</label>
                        <div class="col-sm-10">
                            <input type="text" name="kriteria" value="<?=($formType=='edit')?$kriteria['kriteria']:''?>" id="kriteria" placeholder="Kriteria" class="form-control" required/>
                        </div>
                    </div>
                    <button type="submit" class="btn btn-success"><?=($formType=='add')?'Tambah':'Edit'?></button>
                    <a href="<?=url('?page=spk.setting&subPage=kriteria')?>" class="btn btn-danger">Batal</a>
                </form>
            </div>
        </div>
    </div>
</div>
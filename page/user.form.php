<?php
    // Form Setup
    $formType = 'add';
    $id = '';
    $user = new User();
    if(isset($_GET['id']))
    {
        $formType = 'edit';
        $id = $_GET['id'];
        $user = $user->find($id);
        if(!$user)
        {
            $session->setSession('warning', 'User ID Not Found.');
            header('location:'.url('/'));
        }
    }
?>
<!-- Breadcrumb -->
<ol class="breadcrumb bg-white">
    <li class="breadcrumb-item"><a href="<?=url('/')?>">Home</a></li>
    <li class="breadcrumb-item"><a href="<?=url('?page=user')?>">Admin</a></li>
    <li class="breadcrumb-item active"><?=($formType=='add')?'New':$user['name']?></li>
</ol>
<!-- Content -->
<div class="card">
    <div class="card-body">
        <div class="row">
            <div class="col-12">
                <form action="?page=user" method="POST">
                    <input type="hidden" name="model" value="user"/>
                    <input type="hidden" name="<?=$formType?>" value="true"/>
                    <div class="form-group row" style="display:none;">
                        <label class="col-sm-2 col-form-label" for="user_id">User ID</label>
                        <div class="col-sm-10">
                            <input type="text" name="user_id" value="<?=$id?>" id="user_id" placeholder="User ID" class="form-control" readonly/>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label" for="name">Name</label>
                        <div class="col-sm-10">
                            <input type="text" name="name" value="<?=($formType=='edit')?$user['name']:''?>" id="name" placeholder="Name" class="form-control" autofocus required/>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label" for="username">Username</label>
                        <div class="col-sm-10">
                            <input type="username" name="username" value="<?=($formType=='edit')?$user['username']:''?>" id="username" placeholder="Username" class="form-control" required/>
                        </div>
                    </div>
                    <?php
                        if($formType=='edit')
                        {
                    ?>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label" for="current_password">Current Password</label>
                            <div class="col-sm-10">
                                <input type="password" name="current_password" id="current_password" placeholder="Current Password" class="form-control"/>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label" for="password">New Password</label>
                            <div class="col-sm-10">
                                <input type="password" name="password" id="password" placeholder="New Password" class="form-control"/>
                            </div>
                        </div>
                    <?php
                        }else{
                    ?>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label" for="password">Password</label>
                            <div class="col-sm-10">
                                <input type="password" name="password" id="password" placeholder="Password" class="form-control" required/>
                            </div>
                        </div>
                    <?php
                        }
                    ?>
                    <button type="submit" class="btn btn-success"><?=($formType=='add')?'Tambah':'Edit'?></button>
                    <a href="<?=url('?page=user')?>" class="btn btn-danger">Batal</a>
                </form>
            </div>
        </div>
    </div>
</div>
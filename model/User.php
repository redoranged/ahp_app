<?php
require_once 'Model.php';

class User extends Model
{
    public $name = 'User';
    public $table = 'admins';
    public $primaryKey = 'user_id';
    protected $columns = ['name', 'username', 'password'];
    protected $hide = ['password'];
    protected $many2one = [];
    protected $one2many = [];

    public function login($username, $password)
    {
        $user = $this->select('WHERE username="'.$username.'"');

        if(count($user) <= 0)
        {
            $this->sessionError("User Not Found!");
            return false;
        }else{
            $user = $this->select('WHERE username="'.$username.'" AND password="'.md5($password).'"');
            if(count($user) <= 0)
            {
                $this->sessionError("Wrong Password!");
                return false;
            }else{
                return $user[0];
            }
        }
        return false;
    }

    public function checkPassword($user_id, $password)
    {
        $user = $this->select('WHERE user_id="'.$user_id.'"');

        if(count($user) <= 0)
        {
            return false;
        }else{
            $user = $this->select('WHERE user_id="'.$user_id.'" AND password="'.md5($password).'"');
            if(count($user) <= 0)
            {
                return false;
            }else{
                return $user[0];
            }
        }
        return false;
    }

    /**
     * Basic Function
     */
    public function select($conditions = null)
    {
        $columns = $this->getColumns();
        $query = 'SELECT '.$this->columnsToString($columns).' FROM '.$this->table;
        
        if(!is_null($conditions))
        {
            $query = $query.' '.$conditions;
        }

        $result = $this->db->query($query); 

        if($this->db->error)
        {
            $this->sessionError("MySQL Error: ".$this->db->error);
            return false;
        }

        $res = [];
        while ($row=$result->fetch_assoc())
        {
            foreach($this->many2one as $j)
            {
                $row[$j['class']->table] = $j['class']->select('WHERE '.$j['pkey'].'='.$row[$j['fkey']])[0];
            }
            foreach($this->one2many as $j)
            {
                $row[$j['class']->table] = $j['class']->select('WHERE '.$j['fkey'].'='.$row[$this->primaryKey]);
            }
            $res[] = $row;
        }
        
		return $res;
    }

    public function find($id)
    {
        return $this->select('WHERE '.$this->primaryKey.'='.$id)[0];
    }

    public function create($array)
    {
        $query = 'INSERT INTO '.$this->table.' SET password="'.md5($array['password']).'", '.$this->queryColumn($array, ', ', false);
        
        $this->db->query($query); 

        if($this->db->error)
        {
            $this->sessionError("MySQL Error: ".$this->db->error);
            return false;
        }
        
        $data = $this->select('WHERE '.$this->queryColumn($array, ' AND '));

        if($this->db->error)
        {
            $this->sessionError("MySQL Error: ".$this->db->error);
            return false;
        }

		return $data[0];
    }
    public function update($id, $array)
    {
        if(isset($array['password']))
        {
            $query = 'UPDATE '.$this->table.' SET password="'.md5($array['password']).'", '.$this->queryColumn($array).' WHERE '.$this->primaryKey.'='.$id;
        }else{
            $query = 'UPDATE '.$this->table.' SET '.$this->queryColumn($array).' WHERE '.$this->primaryKey.'='.$id;
        }
        
        $this->db->query($query); 

        if($this->db->error)
        {
            $this->sessionError("MySQL Error: ".$this->db->error);
            return false;
        }
        
        $data = $this->select('WHERE '.$this->primaryKey.'='.$id);

        if($this->db->error)
        {
            $this->sessionError("MySQL Error: ".$this->db->error);
            return false;
        }

		return $data[0];
    }

    public function delete($id)
    {
        $query = 'DELETE FROM '.$this->table.' WHERE '.$this->primaryKey.'='.$id;
        $this->db->query($query); 

        if($this->db->error)
        {
            $this->sessionError("MySQL Error: ".$this->db->error);
            return false;
        }

        return true;
    }

    function queryColumn($array, $delimiter = ', ', $is_update = true)
    {
        foreach($this->getColumns(false) as $column)
        {
            if($is_update && $column == 'created_at')
            {
                continue;
            }
            $value = null;
            if(array_key_exists($column, $array))
            {
                $value = $array[$column];
            }
            $res[] = $this->setColumn($column, $value);
        }
        $res = array_filter($res);

        return implode($delimiter, $res);
    }

    function setColumn($key, $value = null)
    {
        if($key == 'created_at' || $key == 'updated_at')
        {
            $value = date('Y-m-d H:i:s');
        }
        if($value==null){
            return '';
        }
        if(!is_numeric($value))
        {
            return $key.'="'.$value.'"';
        }
        return $key.'='.$value;
    }

    function getColumns($primaryKey = true)
    {
        $columns = $this->columns;
        if($primaryKey)
        {
            array_unshift($columns, $this->primaryKey);
        }
        foreach($this->hide as $hide)
        {
            if (($key = array_search($hide, $columns)) !== false) {
                unset($columns[$key]);
            }
        }
        if($this->timestamps)
        {
            $columns[] = 'created_at';
            $columns[] = 'updated_at';
        }
        return $columns;
    }

    function columnsToString($columns)
    {
        return implode(', ', $columns);
    }
}
?>
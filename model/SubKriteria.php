<?php
require_once 'Model.php';

class SubKriteria extends Model
{
    public $name = 'Sub Kriteria';
    public $table = 'sub_kriteria';
    public $primaryKey = 'sub_kriteria_id';
    protected $columns = ['kriteria_id', 'sub_kriteria', 'keterangan', 'nilai_prioritas'];
    protected $hide = [];

    /**
     * Custom Function
     */

    public function count($kriteria_id)
    {
        $query = 'SELECT COUNT(*) AS "count" FROM '.$this->table.' WHERE kriteria_id='.$kriteria_id;
        $result = $this->db->query($query); 

        if($this->db->error)
        {
            $this->sessionError("MySQL Error: ".$this->db->error);
            return false;
        }

        $res = [];
        while ($row=$result->fetch_assoc())
        {
            $res[] = $row;
        }
        
		return $res[0];
    }

    /**
     * Basic Function
     */

    public function select($conditions = null)
    {
        $columns = $this->getColumns();
        $query = 'SELECT '.$this->columnsToString($columns).' FROM '.$this->table;
        
        if(!is_null($conditions))
        {
            $query = $query.' '.$conditions;
        }

        $result = $this->db->query($query); 

        if($this->db->error)
        {
            $this->sessionError("MySQL Error: ".$this->db->error);
            return false;
        }

        $res = [];
        while ($row=$result->fetch_assoc())
        {
            $res[] = $row;
        }
        
		return $res;
    }

    public function find($id)
    {
        return $this->select('WHERE '.$this->primaryKey.'='.$id)[0];
    }

    public function create($array)
    {
        $query = 'INSERT INTO '.$this->table.' SET '.$this->queryColumn($array, ', ', false);
        
        $this->db->query($query); 

        if($this->db->error)
        {
            $this->sessionError("MySQL Error: ".$this->db->error);
            return false;
        }
        
        $data = $this->select('WHERE '.$this->queryColumn($array, ' AND '));

        if($this->db->error)
        {
            $this->sessionError("MySQL Error: ".$this->db->error);
            return false;
        }

		return $data[0];
    }
    public function update($id, $array)
    {
        $query = 'UPDATE '.$this->table.' SET '.$this->queryColumn($array).' WHERE '.$this->primaryKey.'='.$id;
        
        $this->db->query($query); 

        if($this->db->error)
        {
            $this->sessionError("MySQL Error: ".$this->db->error);
            return false;
        }
        
        $data = $this->select('WHERE '.$this->primaryKey.'='.$id);

        if($this->db->error)
        {
            $this->sessionError("MySQL Error: ".$this->db->error);
            return false;
        }

		return $data[0];
    }

    public function delete($id)
    {
        $query = 'DELETE FROM '.$this->table.' WHERE '.$this->primaryKey.'='.$id;
        $this->db->query($query);

        if($this->db->error)
        {
            $this->sessionError("MySQL Error: ".$this->db->error);
            return false;
        }

        return true;
    }

    function queryColumn($array, $delimiter = ', ', $is_update = true)
    {
        foreach($this->getColumns(false) as $column)
        {
            if($is_update && $column == 'created_at')
            {
                continue;
            }
            $value = null;
            if(array_key_exists($column, $array))
            {
                $value = $array[$column];
            }
            $res[] = $this->setColumn($column, $value);
        }
        $res = array_filter($res);

        return implode($delimiter, $res);
    }

    function setColumn($key, $value = null)
    {
        if($key == 'created_at' || $key == 'updated_at')
        {
            $value = date('Y-m-d H:i:s');
        }
        if($value==null){
            return '';
        }
        if(!is_numeric($value))
        {
            return $key.'="'.$value.'"';
        }
        return $key.'='.$value;
    }

    function getColumns($primaryKey = true)
    {
        $columns = $this->columns;
        if($primaryKey)
        {
            array_unshift($columns, $this->primaryKey);
        }
        foreach($this->hide as $hide)
        {
            if (($key = array_search($hide, $columns)) !== false) {
                unset($columns[$key]);
            }
        }
        if($this->timestamps)
        {
            $columns[] = 'created_at';
            $columns[] = 'updated_at';
        }
        return $columns;
    }

    function columnsToString($columns)
    {
        return implode(', ', $columns);
    }
}
?>
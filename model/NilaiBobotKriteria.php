<?php
require_once 'Model.php';
require_once 'Kriteria.php';

class NilaiBobotKriteria extends Model
{
    public $name = 'Nilai Bobot Kriteria';
    public $table = 'nilai_bobot_kriteria';
    public $primaryKey = 'nilai_bobot_kriteria_id';
    protected $columns = ['kriteria_id1', 'bobot', 'kriteria_id2'];
    protected $hide = [];

    /**
     * Custom Function
     */

    public function generateDefaultBobot($truncate = false)
    {
        $kriteria = new Kriteria();
        if($truncate)
        {
            $this->truncate();
        }
        $kriterias = $kriteria->select();
        
        $res = [];

        foreach($kriterias as $key1 => $kriteria1)
        {
            foreach($kriterias as $key2 => $kriteria2)
            {
                $value = [
                    'kriteria_id1' => $kriteria1["kriteria_id"],
                    'kriteria_id2' => $kriteria2["kriteria_id"],
                    'bobot' => 1
                ];

                $select = $this->select('WHERE kriteria_id1='.$kriteria1["kriteria_id"].' AND kriteria_id2='.$kriteria2["kriteria_id"]);
                
                if(count($select) <= 0)
                {
                    $res[] = $this->create($value);
                }
            }
        }
    }

    public function selectBobot($kriteria_id1, $kriteria_id2)
    {
        $res = $this->select('WHERE kriteria_id1='.$kriteria_id1.' AND kriteria_id2='.$kriteria_id2);
        
        if(count($res) > 0)
        {
            return $res[0]['bobot'];
        }else{
            return 0;
        }
    }

    public function selectJumlah($kriteria_id)
    {
        $kriteria = new Kriteria();
        $kriterias = $kriteria->select();
        
        foreach ($kriterias as $key => $kriteria) {
            $kriteria_ids[] = $kriteria['kriteria_id'];
        }
        $query = 'SELECT SUM(bobot) AS "jumlah" FROM '.$this->table.' WHERE kriteria_id2='.$kriteria_id.' AND kriteria_id1 in ('.implode(',',$kriteria_ids).')';

        $result = $this->db->query($query); 

        if($this->db->error)
        {
            $this->sessionError("MySQL Error: ".$this->db->error);
            return false;
        }

        $res = [];
        while ($row=$result->fetch_assoc())
        {
            $res[] = $row;
        }
        
		return $res[0]['jumlah'];
    }

    public function selectNormalisasi($kriteria1, $kriteria2)
    {
        return number_format($this->selectBobot($kriteria1, $kriteria2)/$this->selectJumlah($kriteria2), 2);
    }

    public function selectJumlahNormalisasi($kriteria_id, $kriterias)
    {
        $jumlah = 0;
        foreach($kriterias as $kriteria)
        {
            $jumlah += $this->selectNormalisasi($kriteria_id, $kriteria['kriteria_id']);
        }

        return number_format($jumlah, 2);
    }

    public function selectPrioritas($kriteria_id, $kriterias)
    {
        return number_format($this->selectJumlahNormalisasi($kriteria_id, $kriterias)/count($kriterias), 2);
    }

    public function selectPrioritasKriteria($kriteria_id, $kriterias)
    {
        $prioritas = [];
        foreach($kriterias as $kriteria)
        {
            $prioritas[] = $this->selectPrioritas($kriteria['kriteria_id'], $kriterias);
        }
        return number_format($this->selectPrioritas($kriteria_id, $kriterias)/max($prioritas), 2);
    }

    public function selectMatriksPenjumlahan($kriteria_id1, $kriteria_id2, $kriterias)
    {
        return number_format($this->selectBobot($kriteria_id1, $kriteria_id2)*$this->selectPrioritas($kriteria_id2, $kriterias), 2);
    }

    public function selectJumlahPenjumlahan($kriteria_id, $kriterias)
    {
        $jumlah = 0;
        foreach($kriterias as $kriteria)
        {
            $jumlah += $this->selectMatriksPenjumlahan($kriteria_id, $kriteria['kriteria_id'], $kriterias);
        }

        return number_format($jumlah, 2);
    }

    public function selectHasil($kriteria_id, $kriterias)
    {
        return $this->selectJumlahPenjumlahan($kriteria_id, $kriterias)+$this->selectPrioritas($kriteria_id, $kriterias);
    }

    public function selectJumlahHasil($kriterias)
    {
        $jumlah = 0;
        foreach($kriterias as $kriteria)
        {
            $jumlah += $this->selectHasil($kriteria['kriteria_id'], $kriterias);
        }

        return number_format($jumlah, 2);
    }

    public function selectLambdaMaks($kriterias)
    {
        return number_format($this->selectJumlahHasil($kriterias)/count($kriterias), 2);
    }

    public function selectCI($kriterias)
    {
        return number_format(($this->selectLambdaMaks($kriterias)-count($kriterias))/(count($kriterias)-1), 2);
    }

    public function selectCR($kriterias)
    {
        $ratioIndex = [
            0,
            0,
            0.58,
            0.9,
            1.12,
            1.24,
            1.32,
            1.41,
            1.46,
            1.49
        ];

        return number_format($this->selectCI($kriterias)/$ratioIndex[count($kriterias)-1], 2);
    }

    public function selectKonsistensi($kriterias)
    {
        return ($this->selectCR($kriterias) <= 0.1);
    }

    public function saveNilaiPrioritas()
    {
        $kriteria = new Kriteria();
        $kriterias = $kriteria->select();
        
        $res = [];

        foreach($kriterias as $kriteria)
        {
            $model = new Kriteria();
            $value = [
                'nilai_prioritas' => $this->selectPrioritas($kriteria['kriteria_id'], $kriterias)
            ];

            $res[] = $model->update($kriteria['kriteria_id'], $value);
        }

        return $res;
    }

    /**
     * Basic Function
     */
    public function select($conditions = null)
    {
        $columns = $this->getColumns();
        $query = 'SELECT '.$this->columnsToString($columns).' FROM '.$this->table;
        
        if(!is_null($conditions))
        {
            $query = $query.' '.$conditions;
        }

        $result = $this->db->query($query); 

        if($this->db->error)
        {
            $this->sessionError("MySQL Error: ".$this->db->error);
            return false;
        }

        $res = [];
        while ($row=$result->fetch_assoc())
        {
            $res[] = $row;
        }
        
		return $res;
    }

    public function find($id)
    {
        return $this->select('WHERE '.$this->primaryKey.'='.$id)[0];
    }

    public function truncate()
    {
        $query = 'TRUNCATE TABLE '.$this->table;

        $this->db->query($query); 

        if($this->db->error)
        {
            $this->sessionError("MySQL Error: ".$this->db->error);
            return false;
        }

        return true;
    }

    public function create($array)
    {
        $query = 'INSERT INTO '.$this->table.' SET '.$this->queryColumn($array, ', ', false);
        
        $this->db->query($query); 

        if($this->db->error)
        {
            $this->sessionError("MySQL Error: ".$this->db->error);
            return false;
        }
        
        $data = $this->select('WHERE '.$this->queryColumn($array, ' AND '));

        if($this->db->error)
        {
            $this->sessionError("MySQL Error: ".$this->db->error);
            return false;
        }

		return $data[0];
    }
    public function update($id, $array)
    {
        $query = 'UPDATE '.$this->table.' SET '.$this->queryColumn($array).' WHERE '.$this->primaryKey.'='.$id;
        
        $this->db->query($query); 

        if($this->db->error)
        {
            $this->sessionError("MySQL Error: ".$this->db->error);
            return false;
        }
        
        $data = $this->select('WHERE '.$this->primaryKey.'='.$id);

        if($this->db->error)
        {
            $this->sessionError("MySQL Error: ".$this->db->error);
            return false;
        }

		return $data[0];
    }

    public function delete($id)
    {
        $query = 'DELETE FROM '.$this->table.' WHERE '.$this->primaryKey.'='.$id;
        $this->db->query($query);

        if($this->db->error)
        {
            $this->sessionError("MySQL Error: ".$this->db->error);
            return false;
        }

        return true;
    }

    function queryColumn($array, $delimiter = ', ', $is_update = true)
    {
        foreach($this->getColumns(false) as $column)
        {
            if($is_update && $column == 'created_at')
            {
                continue;
            }
            $value = null;
            if(array_key_exists($column, $array))
            {
                $value = $array[$column];
            }
            $res[] = $this->setColumn($column, $value);
        }
        $res = array_filter($res);

        return implode($delimiter, $res);
    }

    function setColumn($key, $value = null)
    {
        if($key == 'created_at' || $key == 'updated_at')
        {
            $value = date('Y-m-d H:i:s');
        }
        if($value==null){
            return '';
        }
        if(!is_numeric($value))
        {
            return $key.'="'.$value.'"';
        }
        return $key.'='.$value;
    }

    function getColumns($primaryKey = true)
    {
        $columns = $this->columns;
        if($primaryKey)
        {
            array_unshift($columns, $this->primaryKey);
        }
        foreach($this->hide as $hide)
        {
            if (($key = array_search($hide, $columns)) !== false) {
                unset($columns[$key]);
            }
        }
        if($this->timestamps)
        {
            $columns[] = 'created_at';
            $columns[] = 'updated_at';
        }
        return $columns;
    }

    function columnsToString($columns)
    {
        return implode(', ', $columns);
    }
}
?>
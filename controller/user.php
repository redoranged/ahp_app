<?php

if(isset($_POST['add']))
{
    $request = getRequest();
    
    $model = new User();
    $model = $model->create($request);

    if(!empty($model))
    {
        $session->setSession('success', 'Berhasil tambah Admin!');
    }
}

if(isset($_POST['delete']))
{
    $request = getRequest();
    if(!isset($_POST['user_id'])){
        $session->setSession('warning', 'User ID tidak teridentifikasi!');
    }else{
        $model = new User();
        if($model->delete($_POST['user_id']))
        {
            $session->setSession('success', 'Berhasil hapus User!');
        }else{
            $session->setSession('warning', 'Gagal hapus User!');
        }
    }
    
}

if(isset($_POST['edit']))
{
    $request = getRequest();
    if(!isset($_POST['user_id'])){
        $session->setSession('warning', 'User ID tidak teridentifikasi!');
    }else{
        $model = new User();
        if(!$model->checkPassword($_POST['user_id'], $request['current_password']))
        {
            $session->setSession('warning', 'Password Salah!');
        }else{
            $model = $model->update($_POST['user_id'], $request);
    
            if(!empty($model))
            {
                $session->setSession('success', 'Berhasil edit data Admin!');
            }else{
                $session->setSession('warning', 'Gagal edit data Admin!');
            }
        }
    }
}
?>
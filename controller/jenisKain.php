<?php

if(isset($_POST['add']))
{
    $request = getRequest();
    
    $model = new JenisKain();
    $model = $model->create($request);

    if(!empty($model))
    {
        $session->setSession('success', 'Berhasil tambah Jenis Kain!');
    }
}

if(isset($_POST['delete']))
{
    $request = getRequest();
    if(!isset($_POST['jenis_kain_id'])){
        $session->setSession('warning', 'Jenis Kain ID tidak teridentifikasi!');
    }else{
        $model = new JenisKain();
        if($model->delete($_POST['jenis_kain_id']))
        {
            $session->setSession('success', 'Berhasil hapus Jenis Kain!');
        }else{
            $session->setSession('warning', 'Gagal hapus Jenis Kain!');
        }
    }
    
}

if(isset($_POST['edit']))
{
    $request = getRequest();
    if(!isset($_POST['jenis_kain_id'])){
        $session->setSession('warning', 'Jenis Kain ID tidak teridentifikasi!');
    }else{
        $model = new JenisKain();
        $model = $model->update($_POST['jenis_kain_id'], $request);

        if(!empty($model))
        {
            $session->setSession('success', 'Berhasil edit Jenis Kain!');
        }else{
            $session->setSession('warning', 'Gagal edit Jenis Kain!');
        }
    }
}
?>
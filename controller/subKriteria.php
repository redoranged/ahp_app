<?php

if(isset($_POST['add']))
{
    $request = getRequest();
    
    $model = new SubKriteria();
    $model = $model->create($request);

    if(!empty($model))
    {
        $session->setSession('success', 'Berhasil tambah Sub Kriteria!');
    }
}

if(isset($_POST['delete']))
{
    $request = getRequest();
    if(!isset($_POST['sub_kriteria_id'])){
        $session->setSession('warning', 'Sub Kriteria ID tidak teridentifikasi!');
    }else{
        $model = new SubKriteria();
        if($model->delete($_POST['sub_kriteria_id']))
        {
            $session->setSession('success', 'Berhasil hapus Sub Kriteria!');
        }else{
            $session->setSession('warning', 'Gagal hapus Sub Kriteria!');
        }
    }
    
}

if(isset($_POST['edit']))
{
    $request = getRequest();
    if(!isset($_POST['sub_kriteria_id'])){
        $session->setSession('warning', 'Sub Kriteria ID tidak teridentifikasi!');
    }else{
        $model = new SubKriteria();
        $model = $model->update($_POST['sub_kriteria_id'], $request);

        if(!empty($model))
        {
            $session->setSession('success', 'Berhasil edit Sub Kriteria!');
        }else{
            $session->setSession('warning', 'Gagal edit Sub Kriteria!');
        }
    }
}
?>
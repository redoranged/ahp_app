<?php

if(isset($_POST['add']))
{
    $request = getRequest();
    
    $model = new Kriteria();
    $model = $model->create($request);

    if(!empty($model))
    {
        $session->setSession('success', 'Berhasil tambah Kriteria!');
    }
}

if(isset($_POST['delete']))
{
    $request = getRequest();
    if(!isset($_POST['kriteria_id'])){
        $session->setSession('warning', 'Kriteria ID tidak teridentifikasi!');
    }else{
        $model = new Kriteria();
        if($model->delete($_POST['kriteria_id']))
        {
            $session->setSession('success', 'Berhasil hapus Kriteria!');
        }else{
            $session->setSession('warning', 'Gagal hapus Kriteria!');
        }
    }
    
}

if(isset($_POST['edit']))
{
    $request = getRequest();
    if(!isset($_POST['kriteria_id'])){
        $session->setSession('warning', 'Kriteria ID tidak teridentifikasi!');
    }else{
        $model = new Kriteria();
        $model = $model->update($_POST['kriteria_id'], $request);

        if(!empty($model))
        {
            $session->setSession('success', 'Berhasil edit Kriteria!');
        }else{
            $session->setSession('warning', 'Gagal edit Kriteria!');
        }
    }
}
?>
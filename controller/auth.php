<?php
if(isset($_GET['login']))
{
    $request = getRequest();
    $user = new User();
    $user = $user->login($request['username'], $request['password']);

    if(!empty($user))
    {
        $session->setUser($user['user_id'], $user['name'], $user['username']);
        header('location:'.url('/'));
    }
}

if(isset($_GET['logout']))
{
    $request = getRequest();
    $session->destroyUser($request['user_id']);
    header('location:'.url('/?page=login'));
}
?>
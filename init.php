<?php
require_once 'config.php';

define('URL', $protocol.'://'.$url);
define('ROOT', $root_path.$app_name);

define('DB_HOST', $db_host);
define('DB_USER', $db_user);
define('DB_PASS', $db_pass);
define('DB_NAME', $db_name);

require_once 'config/DB.php';
require_once 'config/Session.php';
require_once 'config/function.php';

$session = new Session();

require_once 'model/User.php';
require_once 'model/Kriteria.php';
require_once 'model/SubKriteria.php';
require_once 'model/JenisKain.php';
require_once 'model/NilaiBobotKriteria.php';
require_once 'model/NilaiBobotSubKriteria.php';
require_once 'model/JenisKainNilaiKriteria.php';
?>
-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 07, 2021 at 02:29 AM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.4.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ahp_app`
--

-- --------------------------------------------------------

--
-- Table structure for table `admins`
--

CREATE TABLE `admins` (
  `user_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `created_at` datetime DEFAULT current_timestamp(),
  `updated_at` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admins`
--

INSERT INTO `admins` (`user_id`, `name`, `username`, `password`, `created_at`, `updated_at`) VALUES
(4, 'Bobby Rahman', 'bobbyrhmn', '21232f297a57a5a743894a0e4a801fc3', '2021-01-06 10:48:10', '2021-01-06 10:48:51');

-- --------------------------------------------------------

--
-- Table structure for table `jenis_kain`
--

CREATE TABLE `jenis_kain` (
  `jenis_kain_id` int(11) NOT NULL,
  `jenis_kain` varchar(255) NOT NULL,
  `keterangan` text DEFAULT NULL,
  `hasil_prioritas` float DEFAULT NULL,
  `ranking` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jenis_kain`
--

INSERT INTO `jenis_kain` (`jenis_kain_id`, `jenis_kain`, `keterangan`, `hasil_prioritas`, `ranking`, `created_at`, `updated_at`) VALUES
(11, 'POLYESTER', NULL, 0.26093, 1, '2020-12-19 15:08:09', '2021-01-06 15:08:40');

-- --------------------------------------------------------

--
-- Table structure for table `jenis_kain_nilai_kriteria`
--

CREATE TABLE `jenis_kain_nilai_kriteria` (
  `jenis_kain_nilai_kriteria_id` int(11) NOT NULL,
  `jenis_kain_id` int(11) NOT NULL,
  `kriteria_id` int(11) NOT NULL,
  `sub_kriteria_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jenis_kain_nilai_kriteria`
--

INSERT INTO `jenis_kain_nilai_kriteria` (`jenis_kain_nilai_kriteria_id`, `jenis_kain_id`, `kriteria_id`, `sub_kriteria_id`, `created_at`, `updated_at`) VALUES
(1, 2, 7, 1, '2020-12-19 06:02:38', '2020-12-19 06:02:38'),
(2, 2, 8, 4, '2020-12-19 06:02:38', '2020-12-19 06:02:38'),
(3, 2, 9, 0, '2020-12-19 06:02:38', '2020-12-19 06:02:38'),
(4, 3, 7, 1, '2020-12-19 06:02:53', '2020-12-19 06:02:53'),
(5, 3, 8, 4, '2020-12-19 06:02:53', '2020-12-19 06:02:53'),
(6, 3, 9, 0, '2020-12-19 06:02:53', '2020-12-19 06:02:53'),
(7, 4, 7, 1, '2020-12-19 06:03:57', '2020-12-19 10:54:09'),
(8, 4, 8, 5, '2020-12-19 06:03:57', '2020-12-19 10:54:09'),
(9, 4, 9, 9, '2020-12-19 06:03:57', '2020-12-19 10:54:09'),
(10, 5, 7, 1, '2020-12-19 10:40:19', '2020-12-19 10:40:19'),
(11, 5, 8, 4, '2020-12-19 10:40:19', '2020-12-19 10:40:19'),
(12, 5, 9, 7, '2020-12-19 10:40:19', '2020-12-19 10:40:19'),
(13, 6, 7, 1, '2020-12-19 10:57:07', '2020-12-19 10:57:07'),
(14, 6, 8, 4, '2020-12-19 10:57:07', '2020-12-19 10:57:07'),
(15, 6, 9, 7, '2020-12-19 10:57:07', '2020-12-19 10:57:07'),
(16, 7, 7, 1, '2020-12-19 14:01:44', '2020-12-19 14:01:58'),
(17, 7, 8, 4, '2020-12-19 14:01:44', '2020-12-19 14:01:58'),
(18, 7, 9, 7, '2020-12-19 14:01:44', '2020-12-19 14:01:58'),
(19, 8, 7, 1, '2020-12-19 14:02:08', '2020-12-19 14:02:11'),
(20, 8, 8, 4, '2020-12-19 14:02:08', '2020-12-19 14:02:11'),
(21, 8, 9, 7, '2020-12-19 14:02:08', '2020-12-19 14:02:11'),
(22, 9, 7, 1, '2020-12-19 14:02:55', '2020-12-19 14:02:55'),
(23, 9, 8, 4, '2020-12-19 14:02:55', '2020-12-19 14:02:55'),
(24, 9, 9, 7, '2020-12-19 14:02:55', '2020-12-19 14:02:55'),
(25, 10, 7, 1, '2020-12-19 15:07:54', '2020-12-19 15:07:54'),
(26, 10, 8, 4, '2020-12-19 15:07:54', '2020-12-19 15:07:54'),
(27, 10, 9, 7, '2020-12-19 15:07:54', '2020-12-19 15:07:54'),
(28, 11, 7, 2, '2020-12-19 15:08:09', '2021-01-05 14:13:00'),
(29, 11, 8, 4, '2020-12-19 15:08:09', '2021-01-05 14:13:00'),
(30, 11, 9, 7, '2020-12-19 15:08:09', '2021-01-05 14:13:00');

-- --------------------------------------------------------

--
-- Table structure for table `kriteria`
--

CREATE TABLE `kriteria` (
  `kriteria_id` int(11) NOT NULL,
  `kode_kriteria` varchar(255) NOT NULL,
  `kriteria` varchar(255) NOT NULL,
  `nilai_prioritas` float DEFAULT NULL,
  `created_at` datetime DEFAULT current_timestamp(),
  `updated_at` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kriteria`
--

INSERT INTO `kriteria` (`kriteria_id`, `kode_kriteria`, `kriteria`, `nilai_prioritas`, `created_at`, `updated_at`) VALUES
(7, 'K1', 'Kualitas', 0.57, '2020-12-18 12:48:15', '2021-01-06 15:08:40'),
(8, 'K2', 'Ketebalan', 0.036667, '2020-12-18 13:01:36', '2021-01-06 15:08:40'),
(9, 'K3', 'Harga', 0.196667, '2020-12-18 13:01:44', '2021-01-06 15:08:40');

-- --------------------------------------------------------

--
-- Table structure for table `nilai_bobot_kriteria`
--

CREATE TABLE `nilai_bobot_kriteria` (
  `nilai_bobot_kriteria_id` int(11) NOT NULL,
  `kriteria_id1` int(11) NOT NULL,
  `bobot` float NOT NULL,
  `kriteria_id2` int(11) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `nilai_bobot_kriteria`
--

INSERT INTO `nilai_bobot_kriteria` (`nilai_bobot_kriteria_id`, `kriteria_id1`, `bobot`, `kriteria_id2`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 1, '2020-12-16 12:58:29', '2020-12-16 12:58:29'),
(2, 1, 0.333333, 2, '2020-12-16 12:58:29', '2020-12-16 12:58:33'),
(3, 1, 0.2, 3, '2020-12-16 12:58:29', '2020-12-16 12:58:36'),
(4, 2, 3, 1, '2020-12-16 12:58:29', '2020-12-16 12:58:33'),
(5, 2, 1, 2, '2020-12-16 12:58:29', '2020-12-16 12:58:29'),
(6, 2, 6, 3, '2020-12-16 12:58:29', '2020-12-16 12:58:40'),
(7, 3, 5, 1, '2020-12-16 12:58:29', '2020-12-16 12:58:36'),
(8, 3, 0.166667, 2, '2020-12-16 12:58:29', '2020-12-16 12:58:40'),
(9, 3, 1, 3, '2020-12-16 12:58:29', '2020-12-16 12:58:29'),
(10, 4, 1, 4, '2020-12-18 10:45:40', '2020-12-18 10:45:40'),
(11, 4, 0.111111, 5, '2020-12-18 10:45:40', '2020-12-18 10:46:14'),
(12, 4, 9, 6, '2020-12-18 10:45:40', '2020-12-18 10:46:21'),
(13, 5, 9, 4, '2020-12-18 10:45:40', '2020-12-18 10:46:14'),
(14, 5, 1, 5, '2020-12-18 10:45:40', '2020-12-18 10:45:40'),
(15, 5, 0.111111, 6, '2020-12-18 10:45:40', '2020-12-18 10:46:27'),
(16, 6, 0.111111, 4, '2020-12-18 10:45:40', '2020-12-18 10:46:21'),
(17, 6, 9, 5, '2020-12-18 10:45:40', '2020-12-18 10:46:27'),
(18, 6, 1, 6, '2020-12-18 10:45:40', '2020-12-18 10:45:40'),
(19, 7, 1, 7, '2020-12-18 13:13:40', '2020-12-18 13:13:40'),
(20, 7, 9, 8, '2020-12-18 13:13:40', '2021-01-02 03:51:00'),
(21, 7, 9, 9, '2020-12-18 13:13:40', '2021-01-02 03:51:04'),
(22, 8, 0.111111, 7, '2020-12-18 13:13:40', '2021-01-02 03:51:00'),
(23, 8, 1, 8, '2020-12-18 13:13:40', '2020-12-18 13:13:40'),
(24, 8, 0.111111, 9, '2020-12-18 13:13:40', '2021-01-02 03:51:10'),
(25, 9, 0.111111, 7, '2020-12-18 13:13:40', '2021-01-02 03:51:04'),
(26, 9, 9, 8, '2020-12-18 13:13:40', '2021-01-02 03:51:10'),
(27, 9, 1, 9, '2020-12-18 13:13:40', '2020-12-18 13:13:40'),
(28, 7, 1, 10, '2021-01-05 13:40:58', '2021-01-05 13:40:58'),
(29, 8, 1, 10, '2021-01-05 13:40:58', '2021-01-05 13:40:58'),
(30, 9, 1, 10, '2021-01-05 13:40:58', '2021-01-05 13:40:58'),
(31, 10, 1, 7, '2021-01-05 13:40:58', '2021-01-05 13:40:58'),
(32, 10, 1, 8, '2021-01-05 13:40:58', '2021-01-05 13:40:58'),
(33, 10, 1, 9, '2021-01-05 13:40:58', '2021-01-05 13:40:58'),
(34, 10, 1, 10, '2021-01-05 13:40:58', '2021-01-05 13:40:58');

-- --------------------------------------------------------

--
-- Table structure for table `nilai_bobot_sub_kriteria`
--

CREATE TABLE `nilai_bobot_sub_kriteria` (
  `nilai_bobot_sub_kriteria_id` int(11) NOT NULL,
  `sub_kriteria_id1` int(11) NOT NULL,
  `bobot` float NOT NULL,
  `sub_kriteria_id2` int(11) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `nilai_bobot_sub_kriteria`
--

INSERT INTO `nilai_bobot_sub_kriteria` (`nilai_bobot_sub_kriteria_id`, `sub_kriteria_id1`, `bobot`, `sub_kriteria_id2`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 1, '2020-12-19 04:47:05', '2020-12-19 04:47:05'),
(2, 1, 1, 2, '2020-12-19 04:47:05', '2020-12-19 04:53:49'),
(3, 1, 0.25, 3, '2020-12-19 04:47:05', '2020-12-19 04:53:53'),
(4, 2, 1, 1, '2020-12-19 04:47:05', '2020-12-19 04:53:49'),
(5, 2, 1, 2, '2020-12-19 04:47:05', '2020-12-19 04:47:05'),
(6, 2, 0.2, 3, '2020-12-19 04:47:05', '2020-12-19 04:53:57'),
(7, 3, 4, 1, '2020-12-19 04:47:05', '2020-12-19 04:53:53'),
(8, 3, 5, 2, '2020-12-19 04:47:05', '2020-12-19 04:53:57'),
(9, 3, 1, 3, '2020-12-19 04:47:05', '2020-12-19 04:47:05'),
(10, 4, 1, 4, '2020-12-19 05:02:54', '2020-12-19 05:02:54'),
(11, 4, 0.333333, 5, '2020-12-19 05:02:54', '2020-12-19 05:02:57'),
(12, 4, 0.166667, 6, '2020-12-19 05:02:54', '2020-12-19 05:03:00'),
(13, 5, 3, 4, '2020-12-19 05:02:54', '2020-12-19 05:02:57'),
(14, 5, 1, 5, '2020-12-19 05:02:54', '2020-12-19 05:02:54'),
(15, 5, 7, 6, '2020-12-19 05:02:54', '2020-12-19 05:03:04'),
(16, 6, 6, 4, '2020-12-19 05:02:54', '2020-12-19 05:03:00'),
(17, 6, 0.142857, 5, '2020-12-19 05:02:54', '2020-12-19 05:03:04'),
(18, 6, 1, 6, '2020-12-19 05:02:54', '2020-12-19 05:02:54'),
(19, 7, 1, 7, '2020-12-19 06:03:34', '2020-12-19 06:03:34'),
(20, 7, 0.25, 8, '2020-12-19 06:03:34', '2020-12-19 06:03:37'),
(21, 7, 0.2, 9, '2020-12-19 06:03:34', '2020-12-19 06:03:40'),
(22, 8, 4, 7, '2020-12-19 06:03:34', '2020-12-19 06:03:37'),
(23, 8, 1, 8, '2020-12-19 06:03:34', '2020-12-19 06:03:34'),
(24, 8, 0.25, 9, '2020-12-19 06:03:34', '2020-12-19 06:03:44'),
(25, 9, 5, 7, '2020-12-19 06:03:34', '2020-12-19 06:03:40'),
(26, 9, 4, 8, '2020-12-19 06:03:34', '2020-12-19 06:03:44'),
(27, 9, 1, 9, '2020-12-19 06:03:34', '2020-12-19 06:03:34'),
(28, 2, 1, 10, '2021-01-01 09:40:55', '2021-01-01 09:40:55'),
(29, 3, 1, 10, '2021-01-01 09:40:55', '2021-01-01 09:40:55'),
(30, 10, 1, 2, '2021-01-01 09:40:55', '2021-01-01 09:40:55'),
(31, 10, 1, 3, '2021-01-01 09:40:55', '2021-01-01 09:40:55'),
(32, 10, 1, 10, '2021-01-01 09:40:55', '2021-01-01 09:40:55');

-- --------------------------------------------------------

--
-- Table structure for table `sub_kriteria`
--

CREATE TABLE `sub_kriteria` (
  `sub_kriteria_id` int(11) NOT NULL,
  `kriteria_id` int(11) NOT NULL,
  `sub_kriteria` varchar(255) NOT NULL,
  `keterangan` text DEFAULT NULL,
  `nilai_prioritas` float DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sub_kriteria`
--

INSERT INTO `sub_kriteria` (`sub_kriteria_id`, `kriteria_id`, `sub_kriteria`, `keterangan`, `nilai_prioritas`, `created_at`, `updated_at`) VALUES
(2, 7, 'Bagus', 'nice', 0.395147, '2020-12-19 04:46:57', '2021-01-05 15:20:00'),
(3, 7, 'Cukup Bagus', NULL, 1, '2020-12-19 04:47:02', '2021-01-05 15:20:00'),
(4, 8, 'Tipis', NULL, 0.188718, '2020-12-19 05:02:45', '2020-12-19 05:03:08'),
(5, 8, 'Sedang', NULL, 1, '2020-12-19 05:02:49', '2020-12-19 05:03:08'),
(6, 8, 'Tebal', NULL, 0.446549, '2020-12-19 05:02:52', '2020-12-19 05:03:08'),
(7, 9, 'Murah', NULL, 0.146318, '2020-12-19 06:03:18', '2020-12-19 06:03:47'),
(8, 9, 'Menengah', NULL, 0.390913, '2020-12-19 06:03:27', '2020-12-19 06:03:47'),
(9, 9, 'Mahal', NULL, 1, '2020-12-19 06:03:32', '2020-12-19 06:03:47'),
(10, 7, 'Sangat Bagus', NULL, 0.6341, '2021-01-01 05:25:22', '2021-01-05 15:20:00');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admins`
--
ALTER TABLE `admins`
  ADD PRIMARY KEY (`user_id`);

--
-- Indexes for table `jenis_kain`
--
ALTER TABLE `jenis_kain`
  ADD PRIMARY KEY (`jenis_kain_id`);

--
-- Indexes for table `jenis_kain_nilai_kriteria`
--
ALTER TABLE `jenis_kain_nilai_kriteria`
  ADD PRIMARY KEY (`jenis_kain_nilai_kriteria_id`);

--
-- Indexes for table `kriteria`
--
ALTER TABLE `kriteria`
  ADD PRIMARY KEY (`kriteria_id`),
  ADD UNIQUE KEY `UNIQUE` (`kode_kriteria`);

--
-- Indexes for table `nilai_bobot_kriteria`
--
ALTER TABLE `nilai_bobot_kriteria`
  ADD PRIMARY KEY (`nilai_bobot_kriteria_id`);

--
-- Indexes for table `nilai_bobot_sub_kriteria`
--
ALTER TABLE `nilai_bobot_sub_kriteria`
  ADD PRIMARY KEY (`nilai_bobot_sub_kriteria_id`);

--
-- Indexes for table `sub_kriteria`
--
ALTER TABLE `sub_kriteria`
  ADD PRIMARY KEY (`sub_kriteria_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admins`
--
ALTER TABLE `admins`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `jenis_kain`
--
ALTER TABLE `jenis_kain`
  MODIFY `jenis_kain_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `jenis_kain_nilai_kriteria`
--
ALTER TABLE `jenis_kain_nilai_kriteria`
  MODIFY `jenis_kain_nilai_kriteria_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT for table `kriteria`
--
ALTER TABLE `kriteria`
  MODIFY `kriteria_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `nilai_bobot_kriteria`
--
ALTER TABLE `nilai_bobot_kriteria`
  MODIFY `nilai_bobot_kriteria_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;

--
-- AUTO_INCREMENT for table `nilai_bobot_sub_kriteria`
--
ALTER TABLE `nilai_bobot_sub_kriteria`
  MODIFY `nilai_bobot_sub_kriteria_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;

--
-- AUTO_INCREMENT for table `sub_kriteria`
--
ALTER TABLE `sub_kriteria`
  MODIFY `sub_kriteria_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
